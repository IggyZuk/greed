#include <Logic.h>
#include <App.h>
#include <EventQueue.h>

namespace gw {

Logic::Logic(const App& app) :
            _app(app),
            _entities(),
            _systems(_entities),
            _states(app) {

    //srand(time(0));

    _entities.on_entity_created( [=](ex::Entity entity){ EventQueue::Get().queue<EntityCreatedEvent>(entity); } );
    _entities.on_entity_destroyed( [=](ex::Entity entity){ EventQueue::Get().queue<EntityDestroyedEvent>(entity); } );

    EventQueue::Get().queue<ChangeStateEvent>(ChangeStateEvent::MENU);
}

void Logic::update(const float dt) {

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Z)) {
        EventQueue::Get().queue<ChangeStateEvent>(ChangeStateEvent::MENU);
    } else if(sf::Keyboard::isKeyPressed(sf::Keyboard::X)) {
        EventQueue::Get().queue<ChangeStateEvent>(ChangeStateEvent::GAME);
    }

    _states.update(dt);
}

ex::EntityManager& Logic::entities() { return _entities; }
ex::SystemManager& Logic::systems()  { return _systems; }

}
