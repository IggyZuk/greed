#include <net/Peer.h>
#include <net/Address.h>
#include <net/Packet.h>

namespace gw {
namespace net {

Peer::Peer(ENetPeer* peer) :
            peer(peer) {
}

Address Peer::address() {
    return Address(peer->address);
}

void Peer::setData(void *data) {
    peer->data = data;
}

void* Peer::data() const {
    return peer->data;
}

int Peer::send(enet_uint8 channelID, Packet& packet) {
    // Once the packet is handed over to ENet with enet_peer_send(),
    // ENet will handle its deallocation.
    return enet_peer_send(peer, channelID, packet.packet);
}

void Peer::disconnect(enet_uint32 data) {
    enet_peer_disconnect(peer, data);
}

void Peer::disconnectLater(enet_uint32 data) {
    enet_peer_disconnect_later(peer, data);
}

void Peer::disconnectNow(enet_uint32 data) {
    enet_peer_disconnect_now(peer, data);
}

void Peer::reset() {
    enet_peer_reset(peer);
}

void Peer::ping() {
    enet_peer_ping(peer);
}

Packet Peer::receive(enet_uint8* channelID) {
    return Packet(enet_peer_receive(peer, channelID));
}

int Peer::throttle(enet_uint32 rtt) {
    return enet_peer_throttle(peer, rtt);
}

void Peer::throttleConfigure(enet_uint32 interval, enet_uint32 acceleration, enet_uint32 deceleration) {
    enet_peer_throttle_configure(peer, interval, acceleration, deceleration);
}

}
}
