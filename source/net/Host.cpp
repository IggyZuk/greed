#include <net/Host.h>
#include <net/Event.h>
#include <net/Address.h>
#include <net/Peer.h>
#include <net/Packet.h>

#include <exception>
#include <algorithm>

namespace gw {
namespace net {

Host::Host(const Address& address,
     std::size_t maxClients,
     std::size_t channels,
     enet_uint32 incomingBandwidth,
     enet_uint32 outgoingBandwidth) {

    host = enet_host_create(&address.address, maxClients, channels, incomingBandwidth, outgoingBandwidth);

    if(host == nullptr) {
        throw std::runtime_error("Could not create ENet server!");
    }
}

Host::Host(
     std::size_t maxClients,
     std::size_t channels,
     enet_uint32 incomingBandwidth,
     enet_uint32 outgoingBandwidth) {

    host = enet_host_create(nullptr, maxClients, channels, incomingBandwidth, outgoingBandwidth);

    if(host == nullptr) {
        throw std::runtime_error("Could not create ENet client!");
    }
}

Host::~Host() {
    enet_host_destroy(host);
}

int Host::service(Event& event, enet_uint32 timeoutMillis) {
    int result = enet_host_service(host, &event.event, timeoutMillis);

    handleEvent(event);

    return result;
}

void Host::handleEvent(Event& event) {
    switch (event.type()) {
    case ENET_EVENT_TYPE_CONNECT:
        // Remember the new peer.
        peers.push_back(event.peer().peer);
        break;
    case ENET_EVENT_TYPE_DISCONNECT:
        // This is where you are informed about disconnects.
        // Simply remove the peer from the list of all peers.
        peers.erase(std::find(peers.begin(), peers.end(), event.peer().peer));
        break;
    default:
        break;
    }
}

Peer Host::connect(const Address& address, size_t channelCount) {
    ENetPeer* peer = enet_host_connect(host, &address.address, channelCount, 0);
    if(peer == nullptr)
        throw std::runtime_error("Creating connection failed");

    return Peer(peer);
}

void Host::broadcast(enet_uint8 channelID, Packet& packet) {
    // Once the packet is handed over to ENet with broadcast(),
    // ENet will handle its deallocation.
    enet_host_broadcast(host, channelID, packet.packet);
}

void Host::flush() {
    enet_host_flush(host);
}

void Host::bandwidthThrottle() {
    enet_host_bandwidth_throttle(host);
}

void Host::bandwidthLimit(enet_uint32 incomingBandwidth, enet_uint32 outgoingBandwidth) {
    enet_host_bandwidth_limit(host, incomingBandwidth, outgoingBandwidth);
}

int Host::checkEvents(Event& event) const {
    return enet_host_check_events(host, &event.event);
}

}
}
