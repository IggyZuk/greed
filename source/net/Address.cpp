#include <net/Address.h>
#include <utils/Logger.h>
#include <sstream>

namespace gw {
namespace net {

Address::Address() {}

Address::Address(const ENetAddress& address) :
            address(address) {
}

Address::Address(const char* hostName, int port) {
    setHost(hostName);
    setPort(port);
}

Address::Address(enet_uint32 host, int port) {
    setHost(host);
    setPort(port);
}

void Address::setHost(const char *hostName) {
    enet_address_set_host(&address, hostName);
}

void Address::setHost(enet_uint32 host) {
    address.host = host;
}

enet_uint32 Address::host() const {
    return address.host;
}

void Address::setPort(int port) {
    address.port = port;
}

int Address::port() const {
    return address.port;
}

std::string Address::asString() {
    unsigned char bytes[4];
    bytes[0] = (address.host)       & 0xFF;
    bytes[1] = (address.host >> 8)  & 0xFF;
    bytes[2] = (address.host >> 16) & 0xFF;
    bytes[3] = (address.host >> 24) & 0xFF;

    std::ostringstream ss;
    ss << (int)bytes[0] << "." <<
          (int)bytes[1] << "." <<
          (int)bytes[2] << "." <<
          (int)bytes[3] << ":" <<
          address.port;

    return std::string(ss.str());
}

}
}
