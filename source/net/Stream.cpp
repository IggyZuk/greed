#include <net/Stream.h>
#include <winsock2.h>
#include <cstddef>
#include <cstring>
#include <iterator>
#include <string>
#include <vector>

namespace gw {
namespace net {

Stream::Stream() :
            m_readPos(0),
            m_isValid(true) {
}

Stream::Stream(const void* data, std::size_t sizeInBytes) :
            m_readPos(0),
            m_isValid(true) {
    append(data, sizeInBytes);
}

Stream::~Stream() {
}

void Stream::append(const void* data, std::size_t sizeInBytes) {
    if (data && (sizeInBytes > 0)) {
        std::size_t start = m_data.size();
        m_data.resize(start + sizeInBytes);
        std::memcpy(&m_data[start], data, sizeInBytes);
    }
}

void Stream::clear() {
    m_data.clear();
    m_readPos = 0;
    m_isValid = true;
}

const void* Stream::getData() const {
    return !m_data.empty() ? &m_data[0] : nullptr;
}

std::size_t Stream::getDataSize() const {
    return m_data.size();
}

bool Stream::endOfPacket() const {
    return m_readPos >= m_data.size();
}

Stream::operator BoolType() const {
    return m_isValid ? &Stream::checkSize : nullptr;
}

Stream& Stream::operator >>(bool& data) {
    uint8 value;
    if (*this >> value) data = (value != 0);

    return *this;
}

Stream& Stream::operator >>(int8& data) {
    if (checkSize(sizeof(data))) {
        data = *reinterpret_cast<const int8*>(&m_data[m_readPos]);
        m_readPos += sizeof(data);
    }

    return *this;
}

Stream& Stream::operator >>(uint8& data) {
    if (checkSize(sizeof(data))) {
        data = *reinterpret_cast<const uint8*>(&m_data[m_readPos]);
        m_readPos += sizeof(data);
    }

    return *this;
}

Stream& Stream::operator >>(int16& data) {
    if (checkSize(sizeof(data))) {
        data = ntohs(*reinterpret_cast<const int16*>(&m_data[m_readPos]));
        m_readPos += sizeof(data);
    }

    return *this;
}

Stream& Stream::operator >>(uint16& data) {
    if (checkSize(sizeof(data))) {
        data = ntohs(*reinterpret_cast<const uint16*>(&m_data[m_readPos]));
        m_readPos += sizeof(data);
    }

    return *this;
}

Stream& Stream::operator >>(int32& data) {
    if (checkSize(sizeof(data))) {
        data = ntohl(*reinterpret_cast<const int32*>(&m_data[m_readPos]));
        m_readPos += sizeof(data);
    }

    return *this;
}

Stream& Stream::operator >>(uint32& data) {
    if (checkSize(sizeof(data))) {
        data = ntohl(*reinterpret_cast<const uint32*>(&m_data[m_readPos]));
        m_readPos += sizeof(data);
    }

    return *this;
}

Stream& Stream::operator >>(float& data) {
    if (checkSize(sizeof(data))) {
        data = *reinterpret_cast<const float*>(&m_data[m_readPos]);
        m_readPos += sizeof(data);
    }

    return *this;
}

Stream& Stream::operator >>(double& data) {
    if (checkSize(sizeof(data))) {
        data = *reinterpret_cast<const double*>(&m_data[m_readPos]);
        m_readPos += sizeof(data);
    }

    return *this;
}

Stream& Stream::operator >>(char* data) {
    // First extract string length
    uint32 length = 0;
    *this >> length;

    if ((length > 0) && checkSize(length)) {
        // Then extract characters
        std::memcpy(data, &m_data[m_readPos], length);
        data[length] = '\0';

        // Update reading position
        m_readPos += length;
    }

    return *this;
}

Stream& Stream::operator >>(std::string& data) {
    // First extract string length
    uint32 length = 0;
    *this >> length;

    data.clear();
    if ((length > 0) && checkSize(length)) {
        // Then extract characters
        data.assign(&m_data[m_readPos], length);

        // Update reading position
        m_readPos += length;
    }

    return *this;
}

Stream& Stream::operator >>(wchar_t* data) {
    // First extract string length
    uint32 length = 0;
    *this >> length;

    if ((length > 0) && checkSize(length * sizeof(uint32))) {
        // Then extract characters
        for (uint32 i = 0; i < length; ++i) {
            uint32 character = 0;
            *this >> character;
            data[i] = static_cast<wchar_t>(character);
        }
        data[length] = L'\0';
    }

    return *this;
}

Stream& Stream::operator >>(std::wstring& data) {
    // First extract string length
    uint32 length = 0;
    *this >> length;

    data.clear();
    if ((length > 0) && checkSize(length * sizeof(uint32))) {
        // Then extract characters
        for (uint32 i = 0; i < length; ++i) {
            uint32 character = 0;
            *this >> character;
            data += static_cast<wchar_t>(character);
        }
    }

    return *this;
}

Stream& Stream::operator <<(bool data) {
    *this << static_cast<uint8>(data);
    return *this;
}

Stream& Stream::operator <<(int8 data) {
    append(&data, sizeof(data));
    return *this;
}

Stream& Stream::operator <<(uint8 data) {
    append(&data, sizeof(data));
    return *this;
}

Stream& Stream::operator <<(int16 data) {
    int16 toWrite = htons(data);
    append(&toWrite, sizeof(toWrite));
    return *this;
}

Stream& Stream::operator <<(uint16 data) {
    uint16 toWrite = htons(data);
    append(&toWrite, sizeof(toWrite));
    return *this;
}

Stream& Stream::operator <<(int32 data) {
    int32 toWrite = htonl(data);
    append(&toWrite, sizeof(toWrite));
    return *this;
}

Stream& Stream::operator <<(uint32 data) {
    uint32 toWrite = htonl(data);
    append(&toWrite, sizeof(toWrite));
    return *this;
}

Stream& Stream::operator <<(float data) {
    append(&data, sizeof(data));
    return *this;
}

Stream& Stream::operator <<(double data) {
    append(&data, sizeof(data));
    return *this;
}

Stream& Stream::operator <<(const char* data) {
    // First insert string length
    uint32 length = std::strlen(data);
    *this << length;

    // Then insert characters
    append(data, length * sizeof(char));

    return *this;
}

Stream& Stream::operator <<(const std::string& data) {
    // First insert string length
    uint32 length = static_cast<uint32>(data.size());
    *this << length;

    // Then insert characters
    if (length > 0) append(data.c_str(), length * sizeof(std::string::value_type));

    return *this;
}

Stream& Stream::operator <<(const wchar_t* data) {
    // First insert string length
    uint32 length = std::wcslen(data);
    *this << length;

    // Then insert characters
    for (const wchar_t* c = data; *c != L'\0'; ++c)
        *this << static_cast<uint32>(*c);

    return *this;
}

Stream& Stream::operator <<(const std::wstring& data) {
    // First insert string length
    uint32 length = static_cast<uint32>(data.size());
    *this << length;

    // Then insert characters
    if (length > 0) {
        for (std::wstring::const_iterator c = data.begin(); c != data.end(); ++c)
            *this << static_cast<uint32>(*c);
    }

    return *this;
}

bool Stream::checkSize(std::size_t size) {
    m_isValid = m_isValid && (m_readPos + size <= m_data.size());

    return m_isValid;
}
}

}
