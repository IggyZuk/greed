#include <net/Packet.h>

namespace gw {
namespace net {

Packet::Packet(ENetPacket* packet) :
            packet(packet) {

    if (packet == nullptr) {
        throw std::runtime_error("Could not create packet");
    }
}

Packet::Packet(const void* data, size_t dataLength, enet_uint32 flags) :
            packet(enet_packet_create(data, dataLength, flags)) {

    if (packet == nullptr) {
        throw std::runtime_error("Could not create packet");
    }
}

Packet::Packet(Stream& stream, Reliability flag) :
            packet(enet_packet_create(stream.getData(), stream.getDataSize(), (enet_uint32)flag)) {

    if (packet == nullptr) {
        throw std::runtime_error("Could not create packet");
    }
}

void Packet::destroy() {
    enet_packet_destroy(packet);
}

size_t Packet::size() const {
    return packet->dataLength;
}

const enet_uint8* Packet::data() const {
    return packet->data;
}

int Packet::resize(size_t dataLength) {
    return enet_packet_resize(packet, dataLength);
}

}
}
