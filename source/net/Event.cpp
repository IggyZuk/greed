#include <net/Event.h>
#include <net/Packet.h>
#include <net/Peer.h>

namespace gw {
namespace net {

Event::Event() {}

Packet Event::packet() {
    return Packet(event.packet);
}

Peer Event::peer() {
    return Peer(event.peer);
}

ENetEventType Event::type() const {
    return event.type;
}

enet_uint8 Event::channelID() const {
    return event.channelID;
}

enet_uint32 Event::data() const {
    return event.data;
}

}
}
