#include <net/Net.h>
#include <net/Event.h>
#include <net/Address.h>
#include <net/Packet.h>
#include <net/Stream.h>
#include <net/Peer.h>

#include <App.h>
#include <EventQueue.h>
#include <assert.h>

namespace gw {
namespace net {

Server::Server(Address& address) :
            Host(address, 32, 2, 0, 0),
            teleporter(*this),
            address(address) {

    LOG("+++Server Created", address.asString());

    teleporter.forward<NetworkEvent>();
    teleporter.forward<SetPositionEvent>();
}

bool Server::process() {
    Event event;

    while (service(event, 0)) {
        switch (event.type()) {
        case ENET_EVENT_TYPE_CONNECT: {
            LOG("New Connection!");
            break;

        } case ENET_EVENT_TYPE_RECEIVE: {
            LOG("Received message from the client!");

            Stream stream(event.packet().data(), event.packet().size());
            teleporter.composeEvent(stream);

            event.packet().destroy();
            break;

        } case ENET_EVENT_TYPE_DISCONNECT:
            LOG("Disconnecting!");
            return false;

            break;
        }

    }

    return true;
}

Client::Client(Address& address) :
            Host(1, 2, 57600 / 8, 14400 / 8),
            teleporter(*this),
            address(address) {

    LOG("+++Client Created", address.asString());

    teleporter.forward<ChangeStateEvent>();

    connect(address, 2);
}

bool Client::process() {
    Event event;

    if (!connected) {
        if (service(event, 1000)) {
            switch (event.type()) {
            case ENET_EVENT_TYPE_CONNECT:
                LOG("Client connected to the server!");
                connected = true;
                break;
            }
        } else return false;
    }

    // We are connected to the server, listen for incoming data.
    else if (connected) {
        while (service(event, 0)) {

            switch (event.type()) {
            case ENET_EVENT_TYPE_RECEIVE: {
                LOG("Received message from the server!");

                Stream stream(event.packet().data(), event.packet().size());
                teleporter.composeEvent(stream); // event.peer();

                event.packet().destroy();
                break;

            } case ENET_EVENT_TYPE_DISCONNECT:
                LOG("Disconnecting!");
                return false;

                break;
            }
        }
    }

    return true;
}

Net::Net() :
            running(false),
            host(nullptr),
            mode(UNINITIALIZED) {

    // Before using ANY networking code we must first initialize ENet.
    if (enet_initialize() != 0) {
        throw std::runtime_error("ENet initialization failed");
    }

    // Add a listener for when the game starts, so we'll add a host (server / client).
    EventQueue::Get().subscribe<StartGameEvent>(*this);

    LOG("+Net Created");
}

Net::~Net() {
    // Delete the host.
    delete host;

    // Shutdown ENet, we're done with networking.
    enet_deinitialize();

    LOG("~Net Removed");
}

void Net::start(Mode mode, Address& address) {
    // Make sure we aren't already running a host.
    // Now create a new host and start running it!
    //assert(!running && "Host already running!");

    LOG("Start!!!", address.asString());

    if(running) {
        delete host;
        running = false;
        mode = UNINITIALIZED;
    }

    if(!running) {
        this->mode = mode;
        if(mode == SERVER)        host = new Server(address);
        else if(mode == CLIENT)   host = new Client(address);

        running = true;
    }

}

void Net::update() {
    // Nothing to update since we haven't event started.
    if(!running) return;

    // Process host, if anything goes wrong we throw a network error and stop running.
    if(!host->process()){
        LOG("NETWORK ERROR!!!");
        delete host;
        running = false;
    }

}

Address Net::address() const {
    return Address("127.0.0.1", 7000);
}

bool Net::isServer() const { return mode == SERVER; }
bool Net::isClient() const { return mode == CLIENT; }

void Net::receive(const StartGameEvent& event) {
    start(static_cast<Mode>(event.mode), const_cast<Address&>(event.address));
}

}
}
