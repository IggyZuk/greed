#include <net/Teleporter.h>
#include <net/Host.h>
#include <net/Packet.h>
#include <net/Stream.h>
#include <assert.h>
#include <utils/Logger.h>

namespace gw {
namespace net {

Teleporter::Teleporter(Host& host) :
            host(host) {
    // List of all recorded events
    record<NetworkEvent>(RELIABLE);
    record<ChangeStateEvent>(RELIABLE);
    record<SetPositionEvent>(UNRELIABLE);
}

void Teleporter::decomposeEvent(BaseEvent& event) {
    // Network Protocol - contains event ID.
    Stream stream;
    stream << event.my_family();
    event.serialize(stream);

    assert(stream.getDataSize() > 0 && "Can't send empty streams!");

    // Find mode for event and apply those settings
    std::size_t mode = modeHandle(event.my_family());

    Packet::Reliability reliability;
    if(mode == RELIABLE) reliability = Packet::RELIABLE;
    else if(mode == UNRELIABLE) reliability = Packet::UNRELIABLE;

    Packet packet(stream, reliability);
    host.broadcast(mode, packet);
}

void Teleporter::composeEvent(Stream& data) {
    BaseEvent::Family eventType;
    data >> eventType;

    EventPtr event = EventPtr(eventFactory.Create(eventType));
    event->deserialize(data);

    EventQueue::Get().queue(event);
}

std::size_t& Teleporter::modeHandle(const std::size_t id) {
    if (id >= eventModes.size()) {
        eventModes.resize(id + 1);
        eventModes[id] = 0;
    }
    return eventModes[id];
}

}
}
