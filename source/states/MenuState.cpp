#include <states/MenuState.h>
#include <App.h>
#include <EventQueue.h>
#include <net/Net.h>
#include <GUI.h>
#include <memory.h>
#include <glm/gtc/random.hpp>
#include <SFML/Window.hpp>

namespace gw {

MenuState::MenuState(const App& app) : app(app) {

    EventQueue::Get().subscribe<NetworkEvent>(*this);
    EventQueue::Get().subscribe<SetPositionEvent>(*this);
}

void MenuState::init() {

    shape.setRadius(50.0f);
    shape.setOrigin(sf::Vector2f(shape.getRadius(), shape.getRadius()));

    auto notebook = sfg::Notebook::Create();
    std::weak_ptr<sfg::Notebook> weak_notebook = std::weak_ptr<sfg::Notebook>(notebook);

    // Main box for choosing to either start a server or join as client.
    auto main_box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);

    auto server_btn = sfg::Button::Create("Server");

    server_btn->GetSignal(sfg::Button::OnLeftClick).Connect([=]{
        if(auto n = weak_notebook.lock())
            n->SetCurrentPage(1);
    });

    auto client_btn = sfg::Button::Create("Client");
    client_btn->GetSignal(sfg::Button::OnLeftClick).Connect([=]{
        if(auto n = weak_notebook.lock())
            n->SetCurrentPage(2);
    });

    main_box->Pack(sfg::Label::Create("Server or Client"));
    main_box->Pack(sfg::Separator::Create(sfg::Separator::Orientation::HORIZONTAL));
    main_box->Pack(server_btn);
    main_box->Pack(client_btn);


    // Server box displays your IP adress and lets you start the game as host.
    auto server_box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);

    auto start_btn = sfg::Button::Create("Start");
    start_btn->GetSignal(sfg::Button::OnLeftClick).Connect([=]{
        EventQueue::Get().queue<StartGameEvent>(StartGameEvent::SERVER, app.network().address());
    });

    auto back_btn = sfg::Button::Create("Back");
    back_btn->GetSignal(sfg::Button::OnLeftClick).Connect([=]{
        if(auto n = weak_notebook.lock())
            n->SetCurrentPage(0);
    });

    server_box->Pack(sfg::Label::Create("Your IP is: " + app.network().address().asString()));
    server_box->Pack(sfg::Separator::Create(sfg::Separator::Orientation::HORIZONTAL));
    server_box->Pack(start_btn);
    server_box->Pack(back_btn);

    // Client box allows you to enter IP of the host and join the game.
    auto client_box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);

    auto entry = sfg::Entry::Create("127.0.0.1");

    auto join_btn = sfg::Button::Create("Join");
    join_btn->GetSignal(sfg::Button::OnLeftClick).Connect([=]{
        EventQueue::Get().queue<StartGameEvent>(StartGameEvent::CLIENT, net::Address(entry->GetText().toAnsiString().c_str(), 7000));
    });

    auto back_btn2 = sfg::Button::Create("Back");
    back_btn2->GetSignal(sfg::Button::OnLeftClick).Connect([=]{
        if(auto n = weak_notebook.lock())
            n->SetCurrentPage(0);
    });

    client_box->Pack(sfg::Label::Create("Enter host IP:"));
    client_box->Pack(entry);
    client_box->Pack(sfg::Separator::Create(sfg::Separator::Orientation::HORIZONTAL));
    client_box->Pack(join_btn);
    client_box->Pack(back_btn2);

    // Finally add all pages to the notebook.
    notebook->AppendPage(main_box, sfg::Label::Create("Main"));
    notebook->AppendPage(server_box, sfg::Label::Create("Server"));
    notebook->AppendPage(client_box, sfg::Label::Create("Client"));

    // Main window is created here.
    auto main_window = sfg::Window::Create();
    main_window->SetTitle("GreedWars++");

    main_window->Add(notebook);

    main_window->SetRequisition(sf::Vector2f(app.window().getSize().x * 0.45f, app.window().getSize().y * 0.6f));
    main_window->SetPosition(sf::Vector2f(
            static_cast<float>(app.window().getSize().x / 2 ) - main_window->GetAllocation().width / 2.0f,
            static_cast<float>(app.window().getSize().y / 2 ) - main_window->GetAllocation().height / 2.0f
    ));

    app.gui().add(main_window);

    // Events
    auto event_window = sfg::Window::Create();
    event_window->SetTitle("Events");

    auto event_box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);

    auto event1_btn = sfg::Button::Create("Network Event");
    event1_btn->GetSignal(sfg::Button::OnLeftClick).Connect([=]{
        EventQueue::Get().queue<NetworkEvent>("Iggy is the best!", glm::linearRand(0.0f,1024.0f));
    });

    auto event2_btn = sfg::Button::Create("Change State Event");
    event2_btn->GetSignal(sfg::Button::OnLeftClick).Connect([=] {
        EventQueue::Get().queue<ChangeStateEvent>(ChangeStateEvent::GAME);
    });

    event_box->Pack(event1_btn);
    event_box->Pack(event2_btn);
    event_window->Add(event_box);

    app.gui().add(event_window);

    LOG("+MenuState Created");
}

void MenuState::shutdown() {
    app.gui().removeAll();

    LOG("~MenuState Removed");
}

void MenuState::update(const float dt) {
    app.window().draw(shape);

    if(app.network().isServer()) {
        if(sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
            sf::Vector2i pos = sf::Mouse::getPosition(app.window());
            EventQueue::Get().queue<SetPositionEvent>(pos.x, pos.y);
        }
    }
}

void MenuState::receive(const NetworkEvent& event) {
    LOG(event.text, event.number);
}

void MenuState::receive(const SetPositionEvent& event) {
    shape.setPosition(sf::Vector2f(event.x, event.y));
    shape.setFillColor(sf::Color(((float)event.x/1280.0f)*255.0f, ((float)event.y/720.0f)*255.0f, 255.0f));
}

}
