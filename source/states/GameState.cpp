#include <states/GameState.h>
#include <App.h>
#include <EventQueue.h>
#include <Logic.h>
#include <systems/PlanetSystem.h>
#include <systems/RenderSystem.h>

namespace gw {


// GameState implementation
GameState::GameState(const App& app) : app(app) {}

//TODO: Create an object assembler factory

void GameState::init() {

    // Add event listeners
    EventQueue::Get().subscribe<EntityCreatedEvent>(*this);
    EventQueue::Get().subscribe<EntityDestroyedEvent>(*this);

    // Add necessary systems
    auto& systems =  app.logic().systems();
    systems.add<PlanetSystem>(app);
    systems.add<RenderSystem>(app.window());
    //systems.add<SelectionSystem>();

    /*// GUI STUFF!!!
    auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL, 5.0f);

    auto label = sfg::Label::Create("Buildings");

    auto button1 = sfg::Button::Create("Barracks");
    button1->GetSignal(sfg::Button::OnLeftClick).Connect([=]{
        label->SetText("Barracks");
        EventQueue::Get().queue<ChangeStateEvent>(ChangeStateEvent::MENU);
    });
    auto button2 = sfg::Button::Create("Miner");
    button2->GetSignal(sfg::Button::OnLeftClick).Connect([=]{ label->SetText("Miner"); });
    auto button3 = sfg::Button::Create("Tower");
    button3->GetSignal(sfg::Button::OnLeftClick).Connect([=]{ label->SetText("Tower"); });
    auto button4 = sfg::Button::Create("Air Base");
    button4->GetSignal(sfg::Button::OnLeftClick).Connect([=]{ label->SetText("Air Base"); });

    auto window = sfg::Window::Create();
    window->SetTitle("GreedWars++");
    window->SetRequisition(sf::Vector2f(200.0f, 400.0f));
    window->SetPosition(
            sf::Vector2f(
                    static_cast<float>(1280 / 1.25 ) - window->GetAllocation().width / 2.0f,
                    static_cast<float>(720 / 2 ) - window->GetAllocation().height / 2.0f
            )
    );

    box->Pack(label);
    box->Pack(button1);
    box->Pack(button2);
    box->Pack(button3);
    box->Pack(button4);

    window->Add(box);

    app.GUI().add(window);*/

    LOG("+GameState Created");
}

void GameState::shutdown() {
    /*app.GUI().removeAll();*/

    // Remove all systems!
    auto& systems =  app.logic().systems();
    systems.remove<PlanetSystem>();
    systems.remove<RenderSystem>();

    // Remove all entities! We stored them in a vector of ID's
    for(auto& entity : entities) {
        app.logic().entities().destroy(entity);
    }

    LOG("~GameState Removed");
}

void GameState::update(const float dt) {
    auto& systems =  app.logic().systems();
    systems.update<PlanetSystem>(dt);
    systems.update<RenderSystem>(dt);
}

void GameState::receive(const EntityCreatedEvent& event) { LOG("ENTITY CREATED", event.entity.id()); }
void GameState::receive(const EntityDestroyedEvent& event) { LOG("ENTITY REMOVED", event.entity.id()); }

}
