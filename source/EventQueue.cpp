#include <EventQueue.h>

#include <SFML/System/Clock.hpp>

namespace gw {

BaseEvent::Family BaseEvent::family_counter_ = 0;

EventQueue::EventQueue() :
            _activeQueue(0) {

    LOG("+EventQueue Created");
}

EventQueue::~EventQueue() {
    LOG("~EventQueue Removed");
}

void EventQueue::update(const unsigned short milliseconds) {
    sf::Clock clock;
    unsigned short currentTime = 0;

    // Swap active queues and clear the new queue after the swap
    unsigned short queueToProcess = _activeQueue;
    _activeQueue = (_activeQueue + 1) % 2;
    _queues[_activeQueue].clear();

    // Process the queue
    while (!_queues[queueToProcess].empty()) {
        auto event = _queues[queueToProcess].front();
        _queues[queueToProcess].pop_front();
        emit(event);

        currentTime = clock.getElapsedTime().asMilliseconds();
        if (currentTime > milliseconds) {
            clock.restart();
            break;
        }
    }

    // If we couldn't process all of the events, push the remaining events to the new active queue.
    // Note: To preserve sequencing, go back-to-front, inserting them at the head of the active queue
    if (!_queues[queueToProcess].empty()) {
        while (!_queues[queueToProcess].empty()) {
            auto event = _queues[queueToProcess].back();
            _queues[queueToProcess].pop_back();
            _queues[_activeQueue].push_front(event);
        }
    }

}

void EventQueue::queue(const EventPtr& event) {
    push(event);
}

void EventQueue::push(const EventPtr& event) {
    _queues[_activeQueue].push_back(event);
    LOG("//Push event", event->my_family());
}

void EventQueue::emit(const EventPtr& event) {
    auto& sig = signal_for(event->my_family());
    sig->emit(event.get());
}

}

