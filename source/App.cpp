#include <App.h>
#include <Utils.h>
#include <EventQueue.h>
#include <Logic.h>
#include <net/Net.h>
#include <GUI.h>

namespace gw {

App::App() :
            _window(nullptr),
            _logic(nullptr),
            _net(nullptr),
            _gui(nullptr) {
}

void App::init() {
    _window = new sf::RenderWindow(sf::VideoMode(1280, 720), "GreedWars++", sf::Style::Default);
    _logic  = new Logic(*this);
    _net    = new net::Net();
    _gui    = new GUI(window());
}

void App::loop() {
    const float FPS = 1.0f / 60.0f;  // Frames per second
    const float NTPS = 1.0f / 10.0f; // Network ticks per second

    float frame_time = 0.0f;
    float net_time = 0.0f;

    sf::Clock clock;

    sf::Event event;
    while(_window->isOpen()) {
        while (_window->pollEvent(event)) {

            _gui->input(event);

            switch (event.type) {
            case sf::Event::Closed:
                _window->close();
                break;
            case sf::Event::KeyPressed:
                break;
            default:
                break;
            }
        }

        // Delta time and updates
        float dt = clock.restart().asSeconds();
        frame_time += dt;
        net_time += dt;

        while(frame_time >= FPS){
            frame_time -= FPS;

            _window->clear();

            _logic->update(FPS);
            _gui->update(FPS);

            // Networking update
            while(net_time >= NTPS) {
                net_time -= NTPS;

                _net->update();
            }

            EventQueue::Get().update(20);

            _window->display();
        }
    }
}

void App::shutdown() {
    delete _gui;
    delete _net;
    delete _logic;
    delete _window;
}

sf::RenderWindow& App::window()  const { return *_window; }
Logic&            App::logic()   const { return *_logic; }
net::Net&         App::network() const { return *_net; }
GUI&              App::gui()     const { return *_gui; }

}
