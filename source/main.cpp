#include <App.h>
#include <exception>
#include <utils/Logger.h>

/*
 * Create a new Application object and enter its loop.
 * When requested to exit the loop, we shutdown the application - cleaning it up.
 */

int main() {
    try {
        gw::App app;
        app.init(); //check status: throw error
        app.loop();
        app.shutdown(); //check status: throw error
    } catch (const std::exception& exception) {
        LOG("ERROR", exception.what());
        return 1;
    }
    return 0;
}
