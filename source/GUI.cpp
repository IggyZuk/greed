#include <GUI.h>

namespace gw {

GUI::GUI(sf::RenderWindow& target) :
            target(target) {
    desktop.LoadThemeFromFile("assets/greed.thm");
}

void GUI::add(sfg::Window::Ptr window) {
    desktop.Add(window);
}

void GUI::remove(sfg::Window::Ptr window) {
    desktop.Remove(window);
}

void GUI::removeAll() {
    desktop.RemoveAll();
    desktop = sfg::Desktop();
}

void GUI::input(sf::Event event) {
    desktop.HandleEvent(event);
}

void GUI::update(const float dt) {
    desktop.Update(dt);
    gui.Display(target);
}

}
