#include <StateManager.h>
#include <App.h>
#include <EventQueue.h>

#include <states/MenuState.h>
#include <states/GameState.h>

namespace gw {

StateManager::StateManager(const App& app) :
            app(app),
            currentState(nullptr) {

    EventQueue::Get().subscribe<ChangeStateEvent>(*this);

    LOG("+StateManager Created");
}

StateManager::~StateManager() {
    LOG("~StateManager Removed");
}

void StateManager::change(StatePtr state) {
    if(currentState) {
        currentState->shutdown();
    }
    currentState = std::move(state);
    currentState->init();
}

void StateManager::update(const float dt) {
    if (currentState) currentState->update(dt);
}

void StateManager::receive(const ChangeStateEvent& event) {
    switch(event.state) {
    case ChangeStateEvent::MENU:
        change(std::move(MenuState::Create(app)));
        break;
    case ChangeStateEvent::GAME:
        change(std::move(GameState::Create(app)));
        break;
    }
}

}
