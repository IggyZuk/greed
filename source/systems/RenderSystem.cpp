#include <systems/RenderSystem.h>
#include <components/Components.h>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <EventQueue.h>

namespace gw {

RenderSystem::RenderSystem(sf::RenderWindow& window) :
            window(window) {
}

void RenderSystem::update(ex::EntityManager& es, ex::TimeDelta dt) {

    // Find all entities with a renderable and position
    RenderShapeComponent::Handle shape;
    PositionComponent::Handle position;

    for (ex::Entity entity : es.entities_with_components(shape, position)) {
        shape->shape.get()->setPosition(position->position);
        window.draw(*shape->shape.get());
    }

    // Draw all edges of the planet
    EdgeComponent::Handle edge;
    for (ex::Entity entity : es.entities_with_components(edge)) {
        auto edge_comp = entity.component<EdgeComponent>();

        auto e1 = es.get(edge_comp->prev);
        auto e2 = es.get(edge_comp->next);

        auto pos1 = e1.component<PositionComponent>();
        auto pos2 = e2.component<PositionComponent>();

        sf::Vertex line[] = {
            sf::Vertex(sf::Vector2f(pos1->position.x, pos1->position.y)),
            sf::Vertex(sf::Vector2f(pos2->position.x, pos2->position.y))
        };

        window.draw(line, 2, sf::Lines);
    }

    //TODO: Render texture component
    /*RenderTextureComponent::Handle shape;
    for (ex::Entity entity : es.entities_with_components(shape, position)) {
        shape->shape.get()->setPosition(position->position);
        window.draw(*shape->shape.get());
    }*/

    if(clock.getElapsedTime().asSeconds() > 5.0f)
        EventQueue::Get().queue<ChangeStateEvent>(ChangeStateEvent::MENU);
}

}
