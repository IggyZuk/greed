#include <systems/PlanetSystem.h>
#include <components/Components.h>
#include <App.h>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <memory.h>
#include <glm/glm.hpp>
#include <glm/gtc/random.hpp>
#include <utils/Logger.h>
#include <assert.h>
#include <Logic.h>

namespace gw {

PlanetSystem::PlanetSystem(const App& app) :
        app(app) {

    auto& entities = app.logic().entities();

    // Create an initial circle of edges
    const unsigned int steps = 64;
    assert(steps > 2);

    for (unsigned int i = 0; i < steps; ++i) {

        ex::Entity entity = entities.create();
        edges.push_back(entity.id());

        // Create a graphic to display
        std::unique_ptr<sf::Shape> shape(new sf::CircleShape(4.0f));
        shape->setFillColor(sf::Color::Green);
        shape->setOrigin(sf::Vector2f(3.0f, 3.0f));

        //TODO: Perlin noise instead of linear rand
        // Generate position for a full circle
        sf::Vector2f position;
        float angle = i * (glm::pi<float>() * 2.0f) / static_cast<float>(steps);
        float radius = glm::linearRand(250.0f, 300.0f);
        position.x = glm::cos(angle) * radius + (1280 / 2);
        position.y = glm::sin(angle) * radius + (720 / 2);

        entity.assign<RenderShapeComponent>(std::move(shape));
        entity.assign<PositionComponent>(position);
    }

    // For each vertex add an edge components that links two entities together
    for (unsigned int i = 0; i < edges.size()-1; ++i) {
        auto entity = entities.get(edges[i]);
        entity.assign<EdgeComponent>(edges[i], edges[i+1]);
    }

    // Special case for the last element to complete the circle
    auto last = entities.get(edges[edges.size()-1]);
    last.assign<EdgeComponent>(edges[edges.size()-1], edges[0]);

}

PlanetSystem::~PlanetSystem() {
    for(auto& edge : edges) {
        app.logic().entities().destroy(edge);
    }
}

void PlanetSystem::update(ex::EntityManager& es, ex::TimeDelta dt) {

}

}
