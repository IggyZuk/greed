#ifndef INCLUDE_NET_ADDRESS_H_
#define INCLUDE_NET_ADDRESS_H_

#include <enet/enet.h>
#include <string>

namespace gw {
namespace net {

class Host;
class Peer;

class Address {
    friend class Host;
    friend class Peer;
public:
    Address();
    Address(const ENetAddress& address);
    Address(enet_uint32 host, int port);
    Address(const char* hostName, int port);

    void        setHost(const char* hostName);
    void        setHost(enet_uint32 host);
    enet_uint32 host() const;
    void        setPort(int port);
    int         port() const;
    std::string asString();

private:
    ENetAddress address;
};

}
}

#endif
