#ifndef PACKET_H_
#define PACKET_H_

#include <Utils.h>

/**
 * Utility class to build blocks of data to transfer over the network
 */

namespace gw {
namespace net {

class Stream {

    // A bool-like type that cannot be converted to integer or pointer types
    typedef bool (Stream::*BoolType)(std::size_t);

public:

    /**
     * Default constructor creates an empty packet
     */
    Stream();

    /**
     * Append data to the end of the packet at construction
     */
    Stream(const void* data, std::size_t sizeInBytes);
    virtual ~Stream();

    /**
     * Append data to the end of the packet
     *
     * @data        - Pointer to the sequence of bytes to append
     * @sizeInBytes - Number of bytes to append
     */
    void append(const void* data, std::size_t sizeInBytes);

    /**
     * Clear the packet
     *
     * After calling Clear, the packet is empty.
     */
    void clear();

    /**
     * Get a pointer to the data contained in the packet
     *
     * Warning: the returned pointer may become invalid after
     * you append data to the packet, therefore it should never
     * be stored.
     * The return pointer is NULL if the packet is empty.
     *
     * #return Pointer to the data
     */
    const void* getData() const;

    /**
     * Get the size of the data contained in the packet
     *
     * This function returns the number of bytes pointed to by
     * what getData returns.
     *
     * #return Data size, in bytes
     */
    std::size_t getDataSize() const;

    /**
     * Tell if the reading position has reached the end of the packet
     *
     * This function is useful to know if there is some data
     * left to be read, without actually reading it.
     *
     * #return True if all data was read, false otherwise
     */
    bool endOfPacket() const;

public:

    /**
     * Test the validity of the packet for reading
     *
     * This operator allows to test the packet as a boolean
     * variable, to check if a reading operation was successful.
     *
     * A packet will be in an invalid state if it has no more
     * data to read.
     *
     * This behaviour is the same as standard C++ streams.
     *
     * Usage example:
     *
     *   float x;
     *   packet >> x;
     *   if (packet)
     *   {
     *      // ok, x was extracted successfully
     *   }
     *
     * // -- or --
     *
     *   float x;
     *   if (packet >> x)
     *   {
     *      // ok, x was extracted successfully
     *   }
     *
     * Don't focus on the return type, it's equivalent to bool but
     * it disallows unwanted implicit conversions to integer or
     * pointer types.
     *
     * #return True if last data extraction from packet was successful
     */
    operator BoolType() const;

    /**
     * Overloads of operator >> to read data from the packet
     */
    Stream& operator >>(bool& data);
    Stream& operator >>(int8& data);
    Stream& operator >>(uint8& data);
    Stream& operator >>(int16& data);
    Stream& operator >>(uint16& data);
    Stream& operator >>(int32& data);
    Stream& operator >>(uint32& data);
    Stream& operator >>(float& data);
    Stream& operator >>(double& data);
    Stream& operator >>(char* data);
    Stream& operator >>(std::string& data);
    Stream& operator >>(wchar_t* data);
    Stream& operator >>(std::wstring& data);

    /**
     * Overloads of operator << to write data into the packet
     */
    Stream& operator <<(bool data);
    Stream& operator <<(int8 data);
    Stream& operator <<(uint8 data);
    Stream& operator <<(int16 data);
    Stream& operator <<(uint16 data);
    Stream& operator <<(int32 data);
    Stream& operator <<(uint32 data);
    Stream& operator <<(float data);
    Stream& operator <<(double data);
    Stream& operator <<(const char* data);
    Stream& operator <<(const std::string& data);
    Stream& operator <<(const wchar_t* data);
    Stream& operator <<(const std::wstring& data);

private:

    /**
     * Disallow comparisons between packets
     */
    bool operator ==(const Stream& right) const;
    bool operator !=(const Stream& right) const;

    /**
     * Check if the packet can extract a given number of bytes
     * This function updates accordingly the state of the packet.
     *
     * @size Size to check
     *
     * #return True if @size size bytes can be read from the packet
     */
    bool checkSize(std::size_t size);

private:
    std::vector<char> m_data; // < Data stored in the packet
    std::size_t m_readPos;    // < Current reading position in the packet
    bool m_isValid;           // < Reading state of the packet
};

}
}

#endif
