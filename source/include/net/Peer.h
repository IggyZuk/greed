#ifndef INCLUDE_NET_PEER_H_
#define INCLUDE_NET_PEER_H_

#include <enet/enet.h>

namespace gw {
namespace net {

class Address;
class Packet;
class Host;

class Peer {
    friend Host;
public:
    Peer(ENetPeer* peer);

    Address address();
    void    setData(void* data);
    void*   data() const;
    int     send(enet_uint8 channelID, Packet &packet);
    void    disconnect(enet_uint32 data = 0);
    void    disconnectLater(enet_uint32 data = 0);
    void    disconnectNow(enet_uint32 data = 0);
    void    reset();
    void    ping();
    Packet  receive(enet_uint8* channelID);
    int     throttle(enet_uint32 rtt);
    void    throttleConfigure(enet_uint32 interval, enet_uint32 acceleration, enet_uint32 deceleration);

private:
    ENetPeer* peer;
};

}
}

#endif
