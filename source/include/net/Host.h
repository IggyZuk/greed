#ifndef INCLUDE_NET_HOST_H_
#define INCLUDE_NET_HOST_H_

#include <enet/enet.h>
#include <vector>
#include <cstddef>

namespace gw {
namespace net {

class Event;
class Address;
class Peer;
class Packet;

class Host {
public:
    Host(const Address& address,
         std::size_t maxClients,
         std::size_t channels,
         enet_uint32 incomingBandwidth,
         enet_uint32 outgoingBandwidth);

    Host(
         std::size_t maxClients,
         std::size_t channels,
         enet_uint32 incomingBandwidth,
         enet_uint32 outgoingBandwidth);

    virtual ~Host();

    virtual bool process() = 0;

    int  service(Event& event, enet_uint32 timeoutMillis);
    void handleEvent(Event& event);
    Peer connect(const Address& address, size_t channelCount);
    void broadcast(enet_uint8 channelID, Packet& packet);
    void flush();
    void bandwidthThrottle();
    void bandwidthLimit(enet_uint32 incomingBandwidth, enet_uint32 outgoingBandwidth);
    int  checkEvents(Event& event) const;

protected:
    ENetHost* host;
    std::vector<ENetPeer*> peers;
};

}
}

#endif
