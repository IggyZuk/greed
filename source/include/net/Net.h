#ifndef NET_H_
#define NET_H_

#include <net/Host.h>
#include <net/Teleporter.h>
#include <Events.h>

namespace gw {
namespace net {

class Address;

/*
 * Server is listening for connections.
 * It checks what time of event was given in the packet.
 */
class Server : public Host {
public:
    Server(Address& address);
    bool process();

private:
    Teleporter teleporter;
    Address address;
};

class Client : public Host {
public:
    Client(Address& address);
    bool process();

private:
    Teleporter teleporter;
    Address address;
    bool connected {false};
};

/*
 * Net manager is the main object for networking.
 * It contains a connection host which can either be a server or a client.
 */
class Net : NonCopyable, public Receiver<Net> {
public:

    enum Mode {
        SERVER = 0,
        CLIENT = 1,
        UNINITIALIZED = 2
    };

    Net();
    ~Net();

    void start(Mode mode, Address& address);
    void update();

    Address address() const;

    bool isServer() const;
    bool isClient() const;

    // Event receivers
    void receive(const StartGameEvent& event);

private:
    bool running;
    Host* host;
    Mode mode;
};

}
}

#endif
