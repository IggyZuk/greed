#ifndef INCLUDE_NET_PACKET_H_
#define INCLUDE_NET_PACKET_H_

#include <enet/enet.h>
#include <net/Stream.h>

namespace gw {
namespace net {

class Packet {
    friend class Host;
    friend class Peer;
    friend class Event;
public:
    enum Reliability {
        UNRELIABLE = ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT,
        RELIABLE  = ENET_PACKET_FLAG_RELIABLE
    };
    Packet(ENetPacket* packet);
    Packet(const void* data, size_t dataLength, enet_uint32 flags);
    Packet(Stream& stream, Reliability flag);

    void              destroy();
    size_t            size() const;
    const enet_uint8* data() const;
    int               resize(size_t dataLength);

private:
    ENetPacket* packet;
};

}
}

#endif
