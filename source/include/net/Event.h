#ifndef INCLUDE_NET_EVENT_H_
#define INCLUDE_NET_EVENT_H_

#include <enet/enet.h>

namespace gw {
namespace net {

class Peer;
class Packet;

class Event {
    friend class Host;
public:
    Event();

    Packet        packet();
    Peer          peer();
    ENetEventType type() const;
    enet_uint8    channelID() const;
    enet_uint32   data() const;

private:
    ENetEvent event;
};

}
}

#endif
