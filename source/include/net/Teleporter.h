#ifndef INCLUDE_NET_TELEPORTER_H_
#define INCLUDE_NET_TELEPORTER_H_

#include <EventQueue.h>
#include <utils/GenericObjectFactory.h>
#include <vector>

/**
 * Teleporter allows a host (server or client) to send and receive custom events.
 * All network-able events are recorded with channel options specified.
 *
 * TODO: Allow sending events to a specific peer
 */

namespace gw {
namespace net {

class Host;
class Packet;
class Stream;

class Teleporter : public Receiver<Teleporter> {
public:

    Teleporter(Host& host);

    /**
     *  Turn an event into binary stream
     */
    void decomposeEvent(BaseEvent& event);

    /**
     * Turn binary stream into an event
     */
    void composeEvent(Stream& data);

    /*
     * Subscribe to events which will get forwarded through the network
     * e.g
     *   teleporter.forward<NetworkEvent>();
     */
    template<typename E>
    void forward() {
        EventQueue::Get().subscribe<E>(*this);
    }

    /**
     * Teleporter listens for all kinds of events which it then decomposes and sends over the internet.
     */
    template<typename E>
    void receive(const E& event) {
        //TODO: Push to queue and check for duplicates
        decomposeEvent(const_cast<E&>(event));
    }

private:

    enum Channel {
        RELIABLE = 0,
        UNRELIABLE = 1
    };

    std::size_t& modeHandle(const std::size_t id);

    /**
     * Record an event with the teleporter
     */
    template<typename E>
    void record(Channel channel) {
        eventFactory.Register<E>(E::family());
        modeHandle(E::family()) = channel;
    }

    Host& host;
    std::vector<std::size_t> eventModes;
    GenericObjectFactory<BaseEvent, BaseEvent::Family> eventFactory;

};

}
}

#endif
