#ifndef INCLUDE_COMPONENTS_COMPONENTS_H_
#define INCLUDE_COMPONENTS_COMPONENTS_H_

#include <entityx/entityx.hh>
#include <SFML/Graphics.hpp>
#include <memory.h>

namespace gw {

/**
 * Position is used for everything
 */
struct PositionComponent : ex::Component<PositionComponent> {
    explicit PositionComponent(sf::Vector2f position) :
                position(position) {
    }
    sf::Vector2f position;
};

/**
 *  Component for the rendering primitive shapes in RenderSystem
 */
struct RenderShapeComponent : ex::Component<RenderShapeComponent> {
    explicit RenderShapeComponent(std::unique_ptr<sf::Shape> shape) :
                shape(std::move(shape)) {
    }
    std::unique_ptr<sf::Shape> shape;
};


/**
 * Planet is made up of edges, implemented as a double linked list
 */
struct EdgeComponent : ex::Component<EdgeComponent> {
    explicit EdgeComponent(ex::Entity::Id prev, ex::Entity::Id next) :
                prev(prev),
                next(next) {

    }
    ex::Entity::Id prev;
    ex::Entity::Id next;
    //TODO: Add level & normal to EdgeComponent
};

}

#endif
