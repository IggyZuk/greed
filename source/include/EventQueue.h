#ifndef EVENTQUEUE_H_
#define EVENTQUEUE_H_

#include <utils/Singleton.h>
#include <utils/Logger.h>
#include <Events.h>

namespace gw {

/**
 * Handles event subscription and delivery.
 *
 * Subscriptions are automatically removed when receivers are destroyed.
 *
 * eg.
 *
 * std::shared_ptr<EventQueue> em = new EventQueue();
 * em->queue<Explosion>(10);
 */
class EventQueue : public Singleton<EventQueue> {
public:

    EventQueue();
    ~EventQueue();
    /**
     * Emits events from the queue if there is time left.
     * All event are emmited to the subscribed receivers.
     *
     * argument is the maximum amount of milliseconds it can take per update
     */
    void update(const unsigned short milliseconds);

    /**
     * Queue an already constructed event.
     */
    void queue(const EventPtr& event);

    /**
     * Add event to queue for further processing.
     *
     * eg.
     *     EventManager em = new EventManager();
     *     em.queue<CollisionEvent>("Car1", "Car2");
     */
    template<typename E, typename ... Args>
    void queue(Args && ... args) {
        auto eventPtr = EventPtr(new E(std::forward<Args>(args) ...));
        push(eventPtr);
    }

    /**
     * Subscribe an object to receive events of type E.
     *
     * Receivers must be subclasses of Receiver and must implement a receive() method accepting the given event type.
     *
     * eg.
     *     struct ExplosionReceiver : public Receiver<ExplosionReceiver> {
     *         void receive(const Explosion &explosion) {
     *         }
     *     };
     *
     *     ExplosionReceiver receiver;
     *     em.subscribe<Explosion>(receiver);
     */
    template<typename E, typename Receiver>
    void subscribe(Receiver& receiver) {
        void (Receiver::*receive)(const E&) = &Receiver::receive;
        auto sig = signal_for(E::family());
        auto wrapper = EventCallbackWrapper<E>(std::bind(receive, &receiver, std::placeholders::_1));
        auto connection = sig->connect(wrapper);
        BaseReceiver &base = receiver;
        base._connections.push_back(std::make_pair(EventSignalWeakPtr(sig), connection));
        LOG("Subscribed to Event", E::family());
    }

    /**
     * Unsubsribe an object from events of type E.
     *
     * e.g.
     *     em.unsubscribe<Explosion>(*this);
     */
    template<typename E, typename Receiver>
    void unsubscribe(Receiver& receiver) {
        EventSignalPtr sig = _handlers[E::family()];
        BaseReceiver &base = receiver;
        base.disconnect(sig);

    }

    std::size_t connected_receivers() const {
        std::size_t size = 0;
        for (EventSignalPtr handler : _handlers) {
            if (handler) size += handler->size();
        }
        return size;
    }

private:

    void push(const EventPtr& event);
    void emit(const EventPtr& event);

    EventSignalPtr& signal_for(const std::size_t id) {
        if (id >= _handlers.size()) _handlers.resize(id + 1);
        if (!_handlers[id]) _handlers[id] = std::make_shared<EventSignal>();
        return _handlers[id];
    }

    // Functor used as an event signal callback that casts to E.
    template<typename E>
    struct EventCallbackWrapper {
        EventCallbackWrapper(std::function<void(const E&)> callback) : callback(callback) {}
        void operator()(const BaseEvent* event) {
            callback(*(static_cast<const E*>(event)));
        }
        std::function<void(const E&)> callback;
    };

private:
    unsigned int _activeQueue;
    std::list<EventPtr> _queues[2];
    std::vector<EventSignalPtr> _handlers;
};

}

#endif
