#ifndef LOGIC_H_
#define LOGIC_H_

#include <StateManager.h>
#include <entityx/Entity.hh>
#include <entityx/extra/System.hh>

/* All logic, and game related sub systems are contained in this class. */

namespace gw {

class App;

class Logic : NonCopyable {
public:
    Logic(const App& app);

    void update(const float dt);

    ex::EntityManager& entities();
    ex::SystemManager& systems();

private:
    const App& _app;

    /**
     * Logic contains all entities and systems.
     * Add new entities and systems here.
     */
    ex::EntityManager _entities;
    ex::SystemManager _systems;

    StateManager _states;
};

}

#endif
