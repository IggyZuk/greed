#ifndef DEBUGGUI_H_
#define DEBUGGUI_H_

#include <SFGUI/SFGUI.hpp>

namespace gw {
class GUI {
public:
    GUI(sf::RenderWindow& window);

    void add(sfg::Window::Ptr window);
    void remove(sfg::Window::Ptr window);
    void removeAll();

    void input(sf::Event event);
    void update(const float dt);

private:
    sf::RenderWindow& target;

    sfg::SFGUI   gui;
    sfg::Desktop desktop;
};

}

#endif
