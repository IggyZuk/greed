#ifndef INCLUDE_SYSTEMS_PLANETSYSTEM_H_
#define INCLUDE_SYSTEMS_PLANETSYSTEM_H_

#include <entityx/entityx.hh>
#include <vector>

/*
 * Planet system works on all ground entities
 */

namespace gw {
class App;

class PlanetSystem : public ex::System<PlanetSystem> {
public:
    PlanetSystem(const App& app);
    ~PlanetSystem();

    void update(ex::EntityManager& es, ex::TimeDelta dt) override;

private:
    const App& app;
    std::vector<ex::Entity::Id> edges;
};

}

#endif
