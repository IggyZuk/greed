#ifndef INCLUDE_SYSTEMS_RENDERSYSTEM_H_
#define INCLUDE_SYSTEMS_RENDERSYSTEM_H_

#include <entityx/entityx.hh>
#include <memory.h>
#include <SFML/System/Clock.hpp>
#include <SFML/System/Vector2.hpp>

namespace sf {
    class Shape;
    class RenderWindow;
}

namespace gw {

/**
 * RenderSystem find all entities with Renderable component and draw it to the screen
 */
class RenderSystem : public ex::System<RenderSystem> {
public:
    RenderSystem(sf::RenderWindow& window);
    void update(ex::EntityManager& es, ex::TimeDelta dt) override;

private:
    sf::RenderWindow& window;
    sf::Clock clock;
};

}

#endif
