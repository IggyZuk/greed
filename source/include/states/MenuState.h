#ifndef MENUSTATE_H_
#define MENUSTATE_H_

#include <StateManager.h>
#include <Events.h>
#include <SFML/Graphics.hpp>

namespace gw {

class MenuState : public State<MenuState>, public Receiver<MenuState> {
public:
    MenuState(const App& app);

    void init();
    void shutdown();

    void update(const float dt);

    void receive(const NetworkEvent& event);
    void receive(const SetPositionEvent& event);

private:
    const App& app;

    sf::CircleShape shape;
};


}

#endif
