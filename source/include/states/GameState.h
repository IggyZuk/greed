#ifndef GAMESTATE_H_
#define GAMESTATE_H_

#include <StateManager.h>

namespace gw {

class GameState : public State<GameState>, public Receiver<GameState> {
public:
    GameState(const App& app);

    void init();
    void shutdown();

    void update(const float dt);

    void receive(const EntityCreatedEvent& event);
    void receive(const EntityDestroyedEvent& event);

private:
    const App& app;

    std::vector<ex::Entity::Id> entities;
};

}

#endif
