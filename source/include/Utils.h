#ifndef UTILS_H_
#define UTILS_H_
/*
 * This is a list of Util functions, should be moved to precompiled headers later on.
 */

// Main libraries
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <entityx/entityx.hh>
#include <entityx/extra/System.hh>

#include <glm/glm.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtc/constants.hpp>

// Standard C++ libraries
#include <windows.h>
#include <iostream>
#include <string>
#include <stdexcept>
#include <cstdlib>
#include <sstream>
#include <vector>
#include <fstream>
#include <memory>


#include <utils/Logger.h>
#include <utils/Types.h>
#include <utils/NonCopyable.h>

namespace ex = entityx;
//namespace enet = enetpp;

#define COUNT_OF(x) ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))


// Smart pointer typedefs
template <typename T>
using ptr = std::shared_ptr<T>;
template <typename T>
using weak_ptr = std::weak_ptr<T>;
template <typename T, typename U>
ptr<U> static_pointer_cast(const ptr<T> &ptr) {
  return std::static_pointer_cast<U>(ptr);
}
template <typename T>
using enable_shared_from_this = std::enable_shared_from_this<T>;

// returns the full path to the file `fileName` in the resources directory of the app bundle
inline std::string ResourcePath(std::string fileName) {
    char executablePath[1024] = {'\0'};
    DWORD charsCopied = GetModuleFileName(NULL, executablePath, 1024);
    if(charsCopied > 0 && charsCopied < 1024) {
        return std::string(executablePath) + "\\..\\..\\assets\\" + fileName;
    } else {
        throw std::runtime_error("GetModuleFileName failed a bit");
    }
}

template <typename T>
std::string NumberToString ( T Number ) {
	std::ostringstream ss;
	ss << Number;
	return ss.str();
}

#endif
