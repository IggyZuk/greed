#ifndef APP_H_
#define APP_H_

#include <utils/NonCopyable.h>

/* App is the main entry object. */

namespace sf { class RenderWindow; }

namespace gw {

class Logic;
class GUI;
namespace net { class Net; }

class App : NonCopyable {
public:
    App();

    void init();
    void loop();
    void shutdown();

    sf::RenderWindow& window()  const;
    Logic&            logic()   const;
    net::Net&         network() const;
    GUI&              gui()     const;

private:
    sf::RenderWindow* _window;
    Logic* 	          _logic;
    net::Net*         _net;
    GUI*              _gui;
};

}

#endif
