#ifndef INCLUDE_UTILS_NONCOPYABLE_H_
#define INCLUDE_UTILS_NONCOPYABLE_H_

class NonCopyable {
protected:
    NonCopyable() = default;
    ~NonCopyable() = default;
    NonCopyable(const NonCopyable&) = delete;
    NonCopyable& operator =(const NonCopyable&) = delete;
};

#endif
