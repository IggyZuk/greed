#ifndef UTILS_LOGGER_H_
#define UTILS_LOGGER_H_

#include <iostream>

template <typename T>
void LOG(const T& value) {
    std::cout << value << std::endl;
}

template <typename U, typename... T>
void LOG(const U& head, const T&... tail) {
    std::cout << head << "; ";
    LOG(tail...);
}

#endif
