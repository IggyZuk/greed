#ifndef INCLUDE_UTILS_SINGLETON_H_
#define INCLUDE_UTILS_SINGLETON_H_

#include <utils/NonCopyable.h>
#include <assert.h>

template<class T>
class Singleton : NonCopyable {
public:
    static T& Get() {
        static T singleton;
        return singleton;
    }
};

#endif
