#ifndef INCLUDE_EVENTS_H_
#define INCLUDE_EVENTS_H_

#include <simplesignal.h>
#include <entityx/Entity.hh>
#include <utils/Logger.h>
#include <utils/Types.h>
#include <net/Stream.h>
#include <net/Address.h>

namespace ex = entityx;

namespace gw {

// Used internally by the EventQueue.
class BaseEvent {
public:
    typedef std::size_t Family;

    virtual ~BaseEvent() {};

    virtual Family my_family() const = 0;

    virtual void serialize(net::Stream& out) {};
    virtual void deserialize(net::Stream& in) {};

protected:
    static Family family_counter_;
};

typedef Simple::Signal<void(const BaseEvent*)> EventSignal;
typedef std::shared_ptr<EventSignal> EventSignalPtr;
typedef std::weak_ptr<EventSignal> EventSignalWeakPtr;
typedef std::shared_ptr<BaseEvent> EventPtr;

/**
 * Event types should subclass from this.
 *
 * struct Explosion : public Event<Explosion> {
 *   Explosion(int damage) : damage(damage) {}
 *   int damage;
 * };
 */
template<typename Derived>
class Event : public BaseEvent {
public:
    // Used internally for registration.
    static Family family() {
        static Family family = family_counter_++;
        return family;
    }

    virtual Family my_family() const override {
        return Derived::family();
    }
    /* TODO - return EventType from ID
    Derived* Type(Event::Family id) {
        return this;
    }*/
};

class BaseReceiver {
public:
    virtual ~BaseReceiver() {
        for (auto connection : _connections) {
            auto &ptr = connection.first;
            if (!ptr.expired()) {
                ptr.lock()->disconnect(connection.second);
                LOG("~Unsubscribed from Event");
            }
        }
    }

    // Return number of signals connected to this receiver.
    std::size_t connected_signals() const {
        std::size_t size = 0;
        for (auto connection : _connections) {
            if (!connection.first.expired()) {
                size++;
            }
        }
        return size;
    }

private:

    void disconnect(const EventSignalPtr& event) {
        for (auto it = _connections.rbegin(); it != _connections.rend(); --it) {
            auto& connection = *it;
            auto& ptr = connection.first;
            if (!ptr.expired()) {
                if (ptr.lock().get() == event.get()) {
                    LOG("Unsubscribed from Event");
                    ptr.lock()->disconnect(connection.second);
                    _connections.erase(--(it.base()));
                }
            }
        }
    }

private:
    friend class EventQueue;
    std::list<std::pair<EventSignalWeakPtr, std::size_t>> _connections;
};

template<typename Derived>
class Receiver : public BaseReceiver {
public:
    virtual ~Receiver() {}
};

/**
 * Events that are used to track creation and destruction of entities and components.
 */
struct EntityCreatedEvent : Event<EntityCreatedEvent> {
    EntityCreatedEvent(ex::Entity entity) : entity(entity) {}
    ex::Entity entity;
};
struct EntityDestroyedEvent : Event<EntityDestroyedEvent> {
    EntityDestroyedEvent(ex::Entity entity) : entity(entity) {}
    ex::Entity entity;
};
/*template<typename C>
struct ComponentAddedEvent : public Event<ComponentAddedEvent<C>> {
    ComponentAddedEvent(ex::Entity entity, ex::ComponentHandle<C> component) :
                entity(entity),
                component(component) {
    }

    ex::Entity entity;
    ex::ComponentHandle<C> component;
};
template<typename C>
struct ComponentRemovedEvent : public Event<ComponentRemovedEvent<C>> {
    ComponentRemovedEvent(ex::Entity entity, ex::ComponentHandle<C> component) :
                entity(entity),
                component(component) {
    }

    ex::Entity entity;
    ex::ComponentHandle<C> component;
};*/


/*
 * Simple event which is used to change the current state.
 * StateManager is subscribed to this event.
 **/
struct ChangeStateEvent : Event<ChangeStateEvent> {
    enum State {
        MENU,
        GAME
    };

    ChangeStateEvent(State state = MENU) : state(state) {};

    void serialize(net::Stream& out) { out << state; };
    void deserialize(net::Stream& in) { in >> state; };

    uint8 state;
};

struct StartGameEvent : Event<StartGameEvent> {
    enum Mode {
        SERVER,
        CLIENT
    };

    StartGameEvent(Mode mode = SERVER, const net::Address& address = net::Address("0", 7000)) :
        mode(mode),
        address(address) {
    };

    uint8 mode;
    net::Address address;
};

struct NetworkEvent : Event<NetworkEvent> {
    NetworkEvent(std::string text = "", uint16 number = 0) :
        text(text),
        number(number) {
    };

    void serialize(net::Stream& out) { out << text << number; };
    void deserialize(net::Stream& in) { in >> text >> number; };

    std::string text;
    uint16 number;
};

struct SetPositionEvent : Event<SetPositionEvent> {
    SetPositionEvent(uint16 x = 0, uint16 y = 0) :
        x(x),
        y(y) {
    };

    void serialize(net::Stream& out) { out << x << y; };
    void deserialize(net::Stream& in) { in >> x >> y; };

    uint16 x;
    uint16 y;
};

}

#endif
