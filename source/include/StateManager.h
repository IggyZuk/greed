#ifndef STATEMANAGER_H_
#define STATEMANAGER_H_

#include <Events.h>

namespace gw {

class App;

/*
 * Base class for all states.
 * State is templated so that we can easily create instances with the call to Create();
 *
 *   e.g.
 *
 *     GameState::Ptr state = GameState::Create(app);
 *
 **/
class BaseState : NonCopyable {
public:
    virtual ~BaseState() {};

    virtual void init() = 0;
    virtual void shutdown() = 0;
    virtual void update(const float dt) = 0;
};

template<typename Derived>
class State : public BaseState {
public:
    typedef std::shared_ptr<Derived> Ptr;
    static Ptr Create(const App& app) {
        return std::make_shared<Derived>(app);
    }
};

/*
 * Maintains current states and continiously updates it.
 * Listens for ChangeStateEvent, so in order to change state do this:
 *
 *   e.g.
 *
 *     App::EventQueue()->queue<ChangeStateEvent>(ChangeStateEvent::GAME);
 *
 **/
class StateManager : public Receiver<StateManager> {
public:

    typedef std::shared_ptr<BaseState> StatePtr;

    StateManager(const App& app);
    ~StateManager();

    void update(const float dt);
    void change(StatePtr state);
    void receive(const ChangeStateEvent& event);

private:
    const App& app;

    StatePtr currentState;
};

}
#endif
