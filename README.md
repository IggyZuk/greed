#![run_nick_run_by_the_titan-d355xxr.gif](https://bitbucket.org/repo/oAXEjx/images/3257740106-run_nick_run_by_the_titan-d355xxr.gif) **GreedWars++** ![run_nick_run_by_the_titan-d355xxr.gif](https://bitbucket.org/repo/oAXEjx/images/3257740106-run_nick_run_by_the_titan-d355xxr.gif)
*****
This is a remake of the game I made back in 2012 for LudumDare.  The theme was *"Tiny World"* and the game was made in roughly 2 days.

Many saw potential in the game, placing it #4 overall, and so I've decided to properly recreate it with a few technical points in mind:

* Online multiplayer of up to 4 players
* Performance
* Use of graphical shaders
* Entity Component System
* Event queue for all communications
*****
![greedwars.jpg](https://bitbucket.org/repo/oAXEjx/images/3153898456-greedwars.jpg)
*****
I've decided to use C++11 as a way to learn more about the practical implementation of the language. I've also put major priority on getting online multiplayer working early.

The current architecture of the project looks as follows where blocks in **bold** are of certain interest:

![architecture.png](https://bitbucket.org/repo/oAXEjx/images/3479322503-architecture.png)